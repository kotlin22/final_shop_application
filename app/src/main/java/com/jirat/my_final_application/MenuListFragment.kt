package com.jirat.my_final_application

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.jirat.my_final_application.ViewModel.ShopApplication
import com.jirat.my_final_application.ViewModel.ShopViewModel
import com.jirat.my_final_application.ViewModel.ShopViewModelFactory
import com.jirat.my_final_application.adapter.ShopListAdapter
import com.jirat.my_final_application.databinding.FragmentMenuListBinding

class MenuListFragment : Fragment() {

    private val viewModel: ShopViewModel by activityViewModels {
        ShopViewModelFactory(
            (activity?.application as ShopApplication).database.MenuDao()
        )
    }

    private var _binding: FragmentMenuListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMenuListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.layoutManager = LinearLayoutManager(this.context)
        val adapter = ShopListAdapter {
            val action = MenuListFragmentDirections.actionMenuListFragmentToItemDetailFragment(it.id)
            this.findNavController().navigate(action)
        }
        binding.recyclerView.adapter = adapter
        viewModel.allItems.observe(this.viewLifecycleOwner) { menu ->
            menu.let {
                adapter.submitList(it)
            }
        }
    }

}