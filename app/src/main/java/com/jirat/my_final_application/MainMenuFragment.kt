package com.jirat.my_final_application

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.jirat.my_final_application.databinding.FragmentMainMenuBinding

class MainMenuFragment : Fragment() {
    private  var _binding: FragmentMainMenuBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainMenuBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.menu?.setOnClickListener{
            val action  = MainMenuFragmentDirections.actionMainMenuFragmentToMenuListFragment()
            view.findNavController().navigate(action)
        }
       binding?.history?.setOnClickListener{
            val action  = MainMenuFragmentDirections.actionMainMenuFragmentToHistoryListFragment()
           view.findNavController().navigate(action)
       }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}