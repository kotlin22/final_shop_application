package com.jirat.my_final_application.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface ToppingDao {
    @Insert
    suspend fun insertTopping(topping: Topping)

    @Update
    suspend fun updateTopping(topping: Topping)

    @Delete
    suspend fun deleteTopping(topping: Topping)

    @Query("SELECT * FROM topping ORDER BY id_top ASC")
    fun getAllTop(): Flow<List<Topping>>

    @Query("SELECT * from topping WHERE id_top = :id")
    fun getItemTop(id: Int): Flow<Topping>

    @Query("SELECT * from topping ORDER BY name_top ASC")
    fun getToppings(): Flow<List<Topping>>

}
