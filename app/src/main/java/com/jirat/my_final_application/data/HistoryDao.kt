package com.jirat.my_final_application.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface HistoryDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertHistory(history: History)

    @Query("SELECT * from history WHERE id = :id")
    fun getHistory(id: Int): Flow<History>

    @Query("SELECT * from history ORDER BY id ASC")
    fun getHistories(): Flow<List<History>>

    @Query("SELECT name from water WHERE id = :id")
    fun getNameHistory(id: Int): Flow<String>

}
