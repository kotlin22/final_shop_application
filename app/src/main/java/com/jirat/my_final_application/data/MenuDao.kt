package com.jirat.my_final_application.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface MenuDao {

    @Query("SELECT * FROM water ORDER BY id ASC")
    fun getAll(): Flow<List<Menu>>

    @Query("SELECT * from water WHERE id = :id")
    fun getItem(id: Int): Flow<Menu>

    @Query("SELECT * from water ORDER BY name ASC")
    fun getItems(): Flow<List<Menu>>

}
