package com.jirat.my_final_application.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity(tableName = "water")
data class Menu(
    @PrimaryKey (autoGenerate = true)
    val id: Int,
    @NotNull @ColumnInfo(name = "name")
    val itemName: String,
    @NotNull @ColumnInfo(name = "price")
    val itemPrice: Double

)

@Entity(tableName = "history",
    foreignKeys = [ForeignKey(
        entity = Menu::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("item_id"),
    )])
data class History(
    @PrimaryKey (autoGenerate = true)
    val id: Int=0,
    @NotNull @ColumnInfo(name = "item_id")
    val idItem: Int,
    @NotNull @ColumnInfo(name = "item_name")
    val ItemName: String,
    @NotNull @ColumnInfo(name = "sale_quantity")
    val Sale: Int,
    @NotNull @ColumnInfo(name = "total_price")
    val Total: Double

)

@Entity(tableName = "topping")
data class Topping(
    @PrimaryKey (autoGenerate = true)
    val id_top: Int,
    @NotNull @ColumnInfo(name = "name_top")
    val itemName_top: String,
    @NotNull @ColumnInfo(name = "price_top")
    val itemPrice_top: Double

)
