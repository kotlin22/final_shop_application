package com.jirat.my_final_application.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Menu::class, History::class, Topping::class], version = 1)
abstract class MenuDatabase : RoomDatabase() {

    abstract fun MenuDao(): MenuDao
    abstract fun HistoryDao(): HistoryDao
    abstract fun ToppingDao(): ToppingDao

    companion object {
        @Volatile
        private var INSTANCE: MenuDatabase? = null
        fun getDatabase(context: Context): MenuDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    MenuDatabase::class.java,
                    "menu_database"
                )
                    .createFromAsset("database/menu.db")
                    .fallbackToDestructiveMigration()
                    .build()

                INSTANCE = instance
                return instance
            }
        }
    }
}