package com.jirat.my_final_application

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.jirat.my_final_application.ViewModel.ShopApplication
import com.jirat.my_final_application.ViewModel.ShopViewModel
import com.jirat.my_final_application.ViewModel.ShopViewModelFactory
import com.jirat.my_final_application.adapter.getFormattedPrice
import com.jirat.my_final_application.data.Menu
import com.jirat.my_final_application.databinding.FragmentItemDetailBinding

class ItemDetailFragment : Fragment() {

    private val viewModel: ShopViewModel by activityViewModels {
        ShopViewModelFactory(
            (activity?.application as ShopApplication).database.MenuDao()
        )
    }
    private val navigationArgs: ItemDetailFragmentArgs by navArgs()
    private var _binding: FragmentItemDetailBinding? = null
    private val binding get() = _binding!!
    lateinit var menu: Menu

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentItemDetailBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.itemId
        viewModel.retrieveItem(id).observe(this.viewLifecycleOwner) { selectedItem ->
            menu = selectedItem
            bind(menu)
        }
        var add = 0
        binding?.btnAddTopping?.setOnClickListener{
            add +=1
        }
        binding?.btnConfirm?.setOnClickListener{
            val num1 = binding?.HowMuch?.text.toString().toIntOrNull()
            val num2 = menu.itemPrice.toInt()
            val result = num2?.let {num1?.times(it)}.toString()
            val quantity = num1.toString()
            if (num1 != null && num1 > 0) {
                if(add > 0){
                    val action = ItemDetailFragmentDirections.actionItemDetailFragmentToCalDetailFragment2(
                        result, quantity, id, sendtext = "+ Black Pearl", service = 5 )
                    view.findNavController().navigate(action)
                }else{
                    val action = ItemDetailFragmentDirections.actionItemDetailFragmentToCalDetailFragment2(
                        result, quantity, id, sendtext = "", service = 0 )
                    view.findNavController().navigate(action)
                }
            }else {
                val toast = Toast.makeText(activity, "!!! Please Input Amount !!!", Toast.LENGTH_SHORT)
                toast.show()
            }
        }
    }

    private fun bind(menu: Menu) {
        binding.apply {
            itemName.text = menu.itemName
            itemPrice.text = menu.getFormattedPrice()
        }
    }


}

