package com.jirat.my_final_application.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jirat.my_final_application.data.Menu
import com.jirat.my_final_application.databinding.ListMenuBinding
import java.text.NumberFormat

fun Menu.getFormattedPrice(): String = NumberFormat.getCurrencyInstance().format(itemPrice)

class ShopListAdapter(private val onItemClicked: (Menu) -> Unit) : ListAdapter<Menu, ShopListAdapter.ItemViewHolder> (DiffCallback) {
    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<Menu>() {
            override fun areItemsTheSame(oldItem: Menu, newItem: Menu): Boolean {
                return oldItem.itemName == newItem.itemName
            }

            override fun areContentsTheSame(oldItem: Menu, newItem: Menu): Boolean {
                return oldItem == newItem
            }
        }
    }

    class ItemViewHolder(private var binding: ListMenuBinding): RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SimpleData")
        fun bind(item: Menu) {
            binding.apply {
                itemName.text = item.itemName
                itemPrice.text = item.getFormattedPrice()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val viewHolder = ItemViewHolder(
            ListMenuBinding.inflate(
                LayoutInflater.from( parent.context),
                parent,
                false
            )
        )
        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.adapterPosition
            onItemClicked(getItem(position))
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val current = getItem(position)
        holder.itemView.setOnClickListener {
            onItemClicked(current)
        }
        holder.bind(current)
    }
}
