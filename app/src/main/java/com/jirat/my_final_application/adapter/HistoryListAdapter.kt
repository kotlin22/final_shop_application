package com.jirat.my_final_application.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jirat.my_final_application.data.History
import com.jirat.my_final_application.databinding.ListHistoryBinding
import java.text.NumberFormat

fun History.getFormattedTotal(): String = NumberFormat.getCurrencyInstance().format(Total)

class HistoryListAdapter(private val onItemClicked: (History) -> Unit) : ListAdapter<History, HistoryListAdapter.ItemViewHolder>(DiffCallback) {
    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<History>() {
            override fun areItemsTheSame(oldItem: History, newItem: History): Boolean {
                return oldItem.Sale == newItem.Sale
            }

            override fun areContentsTheSame(oldItem: History, newItem: History): Boolean {
                return oldItem == newItem
            }
        }
    }

    class ItemViewHolder(private var binding: ListHistoryBinding): RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SimpleData")
        fun bind(item: History) {
            binding.apply {
                itemNameHis.text = item.ItemName
                itemAmountHis.text = item.Sale.toString()
                itemPriceHis.text = item.getFormattedTotal()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val viewHolder = ItemViewHolder(
            ListHistoryBinding.inflate(
                LayoutInflater.from( parent.context),
                parent,
                false
            )
        )
        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.adapterPosition
            onItemClicked(getItem(position))
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val current = getItem(position)
        holder.itemView.setOnClickListener {
            onItemClicked(current)
        }
        holder.bind(current)
    }
}