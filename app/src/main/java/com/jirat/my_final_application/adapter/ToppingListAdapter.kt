package com.jirat.my_final_application.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jirat.my_final_application.data.Topping
import com.jirat.my_final_application.databinding.ListToppingBinding
import java.text.NumberFormat

fun Topping.getFormattedPrice(): String = NumberFormat.getCurrencyInstance().format(itemPrice_top)

class ToppingListAdapter(private val onItemClicked: (Topping) -> Unit) : ListAdapter<Topping, ToppingListAdapter.ItemViewHolder> (DiffCallback) {
    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<Topping>() {
            override fun areItemsTheSame(oldItem: Topping, newItem: Topping): Boolean {
                return oldItem.itemName_top == newItem.itemName_top
            }

            override fun areContentsTheSame(oldItem: Topping, newItem: Topping): Boolean {
                return oldItem == newItem
            }
        }
    }

    class ItemViewHolder(private var binding: ListToppingBinding): RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SimpleData")
        fun bind(item: Topping) {
            binding.apply {
                itemNameTop.text = item.itemName_top
                itemPriceTop.text = item.getFormattedPrice()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val viewHolder = ItemViewHolder(
            ListToppingBinding.inflate(
                LayoutInflater.from( parent.context),
                parent,
                false
            )
        )
        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.adapterPosition
            onItemClicked(getItem(position))
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val current = getItem(position)
        holder.itemView.setOnClickListener {
            onItemClicked(current)
        }
        holder.bind(current)
    }

}
