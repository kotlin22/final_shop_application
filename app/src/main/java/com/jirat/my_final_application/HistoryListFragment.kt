package com.jirat.my_final_application;

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.jirat.my_final_application.ViewModel.*
import com.jirat.my_final_application.adapter.HistoryListAdapter
import com.jirat.my_final_application.databinding.FragmentHistoryListBinding

class HistoryListFragment : Fragment() {

    private val viewModel: HistoryViewModel by activityViewModels {
        HistoryViewModelFactory(
                (activity?.application as ShopApplication).database.HistoryDao()
        )
    }

    private var _binding: FragmentHistoryListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHistoryListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.layoutManager = LinearLayoutManager(this.context)
        val adapter = HistoryListAdapter {
            val action = HistoryListFragmentDirections.actionHistoryListFragmentToMainMenuFragment()
            this.findNavController().navigate(action)
        }
        binding.recyclerView.adapter = adapter
        viewModel.allHistory.observe(this.viewLifecycleOwner) { history ->
                history.let {
            adapter.submitList(it)
        }
        }
    }

}
