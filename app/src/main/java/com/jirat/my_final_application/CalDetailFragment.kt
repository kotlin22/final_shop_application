package com.jirat.my_final_application

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.jirat.my_final_application.ViewModel.*
import com.jirat.my_final_application.data.Menu
import com.jirat.my_final_application.databinding.FragmentCalDetailBinding
import kotlin.properties.Delegates

class CalDetailFragment : Fragment() {

    private val viewModel: ShopViewModel by activityViewModels {
        ShopViewModelFactory(
            (activity?.application as ShopApplication).database.MenuDao()
        )
    }
    private val viewHisModel: HistoryViewModel by activityViewModels {
        HistoryViewModelFactory(
            (activity?.application as ShopApplication).database.HistoryDao()
        )
    }
    private val navigationArgs: CalDetailFragmentArgs by navArgs()
    private var _binding: FragmentCalDetailBinding? = null
    private val binding get() = _binding!!
    private lateinit var result: String
    private lateinit var quantity: String
    private lateinit var sendtext: String
    private var service by Delegates.notNull<Int>()
    lateinit var menu: Menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            result = it.getString(RESULT).toString()
            quantity = it.getString(Quantity).toString()
            sendtext = it.getString(Text).toString()
            service = it.getInt(Price_top)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCalDetailBinding.inflate(inflater, container, false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.itemId
        val extra = service * quantity.toInt()
        val current = result.toInt() + extra
        binding?.total?.text = "Total:"
        binding?.totalPrice?.text = "$current $"
        binding?.quantity?.text = quantity
        binding?.backButton?.setOnClickListener{
            val action = CalDetailFragmentDirections.actionCalDetailFragmentToMenuListFragment()
            view.findNavController().navigate(action)

            viewModel.retrieveItem(id).observe(this.viewLifecycleOwner) { selectedItem ->
                menu = selectedItem
                bind(menu)
                val text_sum = menu.itemName + sendtext
                if(extra > 0){
                    viewHisModel.addNewHistory(menu.id, text_sum, quantity, current.toString())
                }else {
                    viewHisModel.addNewHistory(menu.id, menu.itemName, quantity, current.toString())
                }
            }
        }

    }

    private fun bind(menu: Menu) {
        binding.apply {
            flavor.text = menu.itemName + sendtext
        }
    }
    companion object {
        const val RESULT = "result"
        const val Quantity = "quantity"
        const val Text = "sendtext"
        const val Price_top = "service"
    }
}