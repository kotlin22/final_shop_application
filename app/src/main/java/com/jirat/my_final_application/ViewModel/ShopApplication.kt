package com.jirat.my_final_application.ViewModel

import android.app.Application
import com.jirat.my_final_application.data.MenuDatabase

class ShopApplication : Application(){
    val database: MenuDatabase by lazy { MenuDatabase.getDatabase(this) }
}