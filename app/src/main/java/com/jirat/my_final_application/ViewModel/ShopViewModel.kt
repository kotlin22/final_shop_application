package com.jirat.my_final_application.ViewModel

import androidx.lifecycle.*
import com.jirat.my_final_application.data.Menu
import com.jirat.my_final_application.data.MenuDao
import kotlinx.coroutines.launch

class ShopViewModel(private val MenuDao: MenuDao) : ViewModel() {
    val allItems: LiveData<List<Menu>> = MenuDao.getAll().asLiveData()

    fun retrieveItem(id: Int): LiveData<Menu> {
        return MenuDao.getItem(id).asLiveData()
    }

}

class ShopViewModelFactory(private val MenuDao: MenuDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ShopViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ShopViewModel(MenuDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
