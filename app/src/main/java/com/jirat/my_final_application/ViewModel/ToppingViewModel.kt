package com.jirat.my_final_application.ViewModel

import androidx.lifecycle.*
import com.jirat.my_final_application.data.Topping
import com.jirat.my_final_application.data.ToppingDao
import kotlinx.coroutines.launch

class ToppingViewModel(private val ToppingDao: ToppingDao) : ViewModel() {
    val allTopping: LiveData<List<Topping>> = ToppingDao.getAllTop().asLiveData()


}

class ToppingViewModelFactory(private val ToppingDao : ToppingDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ToppingViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ToppingViewModel(ToppingDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}