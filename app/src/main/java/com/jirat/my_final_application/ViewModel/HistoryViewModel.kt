package com.jirat.my_final_application.ViewModel

import androidx.lifecycle.*
import com.jirat.my_final_application.data.History
import com.jirat.my_final_application.data.HistoryDao
import com.jirat.my_final_application.data.Menu
import kotlinx.coroutines.launch

class HistoryViewModel(private val HistoryDao: HistoryDao) : ViewModel() {

    val allHistory:LiveData<List<History>> = HistoryDao.getHistories().asLiveData()

    private fun insertHistory(history: History) {
        viewModelScope.launch {
            HistoryDao.insertHistory(history)
        }
    }

    private fun getNewHistoryEntry(idItem: Int, ItemName: String, Sale: String, Total: String): History {
        return History(
            idItem = idItem,
            ItemName = ItemName,
            Sale = Sale.toInt(),
            Total = Total.toDouble()
        )
    }

    fun addNewHistory(idItem: Int, ItemName: String,  Sale: String, Total: String) {
        val newItem = getNewHistoryEntry(idItem, ItemName, Sale, Total)
        insertHistory(newItem)
    }

}

class HistoryViewModelFactory(private val history: HistoryDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HistoryViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return HistoryViewModel(history) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
