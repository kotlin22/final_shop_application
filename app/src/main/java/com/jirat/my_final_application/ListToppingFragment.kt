package com.jirat.my_final_application

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.jirat.my_final_application.ViewModel.ShopApplication
import com.jirat.my_final_application.ViewModel.ToppingViewModel
import com.jirat.my_final_application.ViewModel.ToppingViewModelFactory
import com.jirat.my_final_application.databinding.FragmentListToppingBinding


class ListToppingFragment : Fragment() {

    private val viewModel: ToppingViewModel by activityViewModels {
        ToppingViewModelFactory(
            (activity?.application as ShopApplication).database.ToppingDao()
        )
    }

    private var _binding: FragmentListToppingBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentListToppingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        binding.recyclerView.layoutManager = LinearLayoutManager(this.context)
//        val adapter = ToppingListAdapter {
//            val action = MenuListFragmentDirections.actionMenuListFragmentToItemDetailFragment(it.id)
//            this.findNavController().navigate(action)
//        }
//        binding.recyclerView.adapter = adapter
//        viewModel.allTopping.observe(this.viewLifecycleOwner) { topping ->
//            topping.let {
//                adapter.submitList(it)
//            }
//        }
    }
}